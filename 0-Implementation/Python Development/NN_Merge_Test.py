# Using Keras to determine Wine quality
# This script utilises only the CPU of the PC , was completed in a matter of
# seconds
from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense
import numpy

dataset = numpy.genfromtxt("E:\\MCAST - Degree\\Dissertation Work\\Testing Dataset\\winequality-red.csv", delimiter=";", skip_header=1)

input = dataset[:, 0:11]
output = dataset[:, 11]

# Since the data comes in a scale of 0 to 10 , this is needed so we get
# a simple true or false

output = [(round(each / 10)) for each in output]

model = Sequential()
model.add(Dense(20, input_dim=11, init='uniform', activation='relu'))
model.add(Dense(12, init='uniform', activation='relu'))
model.add(Dense(4, init='uniform', activation='relu'))
model.add(Dense(1, init='uniform', activation='sigmoid'))

model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

model.fit(input, output, epochs=10000, batch_size=5000, verbose=2)

model.save('wine-model.h5')

model = load_model('wine-model.h5')

scores = model.evaluate(input, output)

print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
