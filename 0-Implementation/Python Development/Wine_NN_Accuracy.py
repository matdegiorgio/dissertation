from keras.models import load_model
import numpy

dataset = numpy.genfromtxt("E:\\MCAST - Degree\\Dissertation Work\\Testing Dataset\\winequality-white.csv", delimiter=";", skip_header=1)
input = dataset[:, 0:11]
output = dataset[:, 11]

# Since the data comes in a scale of 0 to 10, this is needed
# so we get a simple true or False

output = [(round(each / 10)) for each in output]

model = load_model('wine-model.h5')

scores = model.evaluate(input,output)

print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
